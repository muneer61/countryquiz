<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::group(['prefix'=>'admin'],function(){
    Route::get('/continents','Admin\ContinentController@index')->name('admin.continents');
    Route::post('/continent/create','Admin\ContinentController@store')->name('admin.continent.create');
    Route::get('/continent/delete/{id}','Admin\ContinentController@destroy')->name('admin.continent.delete');
    Route::get('/continent/edit/{id}','Admin\ContinentController@edit')->name('admin.continent.edit');
    Route::post('/continent/update/{id}','Admin\ContinentController@update')->name('admin.continent.update');


    Route::get('/countries','Admin\CountryController@index')->name('admin.countries');
    Route::post('/country/create','Admin\CountryController@store')->name('admin.country.create');
    Route::get('/country/delete/{id}','Admin\CountryController@destroy')->name('admin.country.delete');
    Route::get('/country/edit/{id}','Admin\Contiountryroller@edit')->name('admin.country.edit');
    Route::post('/country/update/{id}','Admin\CountryController@update')->name('admin.country.update');
});


Route::group(['prefix'=>'student'],function(){
   
    Route::get('/test','StudentController@index')->name('test');
    Route::post('/answer/{id}','StudentController@check')->name('check');
    


});



