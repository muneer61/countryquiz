
@extends('layouts.admin.app')


@section('content')


      <form action="{{route('admin.continent.create')}}" method="post">
           {{csrf_field()}}
            
            <input class="form-control" type="text" name="name" id="name" placeholder="continent name">

            <input class="btn btn-primary" type="submit" name="submit" value="create">
      </form>

      <br>

      <table class="table-striped table-bordered">
       <thead>
           <tr>
              <th>id</th>
              <th>name</th>
           </tr>
       </thead>

       <tbody>
          @foreach($continents as $continent)
            <tr>
              <td>{{$continent->id}}</td>
              <td>{{$continent->name}}</td>

              <td><a class="btn btn-danger" href="{{route('admin.continent.edit',['id'=>$continent->id])}}">edit</a></td>
              <td><a class="btn btn-info" href="{{route('admin.continent.delete',['id'=>$continent->id])}}">delete</a></td>
  
            </tr>
          @endforeach
       </tbody>

   </table>
      
@stop