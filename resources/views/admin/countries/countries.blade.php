
@extends('layouts.admin.app')


@section('content')


      <form action="{{route('admin.country.create')}}" method="post">
           {{csrf_field()}}
            
            <input class="form-control" type="text" name="name" id="name" placeholder="country name">

            <select name="continent" class="form-control">
                
                @foreach($continent as $cont)
                <option value="{{$cont->id}}">{{$cont->name}}</option>
                @endforeach
            </select>

            <input class="btn btn-primary" type="submit" name="submit" value="create">
      </form>

      <br>

      <table class="table-striped table-bordered">
       <thead>
           <tr>
              <th>id</th>
              <th>name</th>
           </tr>
       </thead>

       <tbody>
          @foreach($countries as $country)
            <tr>
              <td>{{$country->id}}</td>
              <td>{{$country->name}}</td>

              <td><a class="btn btn-danger" href="{{route('admin.country.edit',['id'=>$country->id])}}">edit</a></td>
              <td><a class="btn btn-info" href="{{route('admin.country.delete',['id'=>$country->id])}}">delete</a></td>
  
            </tr>
          @endforeach
       </tbody>

   </table>
      
@stop