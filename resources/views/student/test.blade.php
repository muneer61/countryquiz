<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <!-- <script>

       var xhr= new XMLHttpRequest();
       xhr.onreadystatechange= function(){
           if (xhr.readyState===4) {
               document.getElementById('resbox').innerHTML=xhr.responseText;
           }
       };

       xhr.open('POST','/student/answer/{id}');
       function giveres(){
           xhr.send();
           document.getElementById('resbtn').style.display="none";
       }
        
    
    
    </script> -->
    <title>Country Quiz</title>
  </head>
  <body>
       
       <div class="container m-4">
           <div class="row">
               <div class="col-lg-12">
                   <div class="card">
                      <div class="card-title p-4">
                           <h3 class="display-5">which continent is this country in?</h3>
                      </div>

                      <hr>

                      <div class="card-body p-4">
                          <h1>{{$country->name}}</h1>
                          
                          <form action="{{route('check',['id'=>$country->continent_id])}}" method="post">
                                   {{csrf_field()}}
                              <select class="form-control" name="ans">
                                  @foreach($continents as $cont)
                                      <option value="{{$cont->id}}">{{$cont->name}}</option>
                                  @endforeach
                              
                              </select>
                              <input class="btn btn-success" type="submit" name="submit" value="answer it!">
                              <!-- <button class="btn btn-success" id="resbtn" onclick="giveres()">answer it!</button> -->
                          </form>

                      </div>
                   
                   </div>
               
               </div>
           </div>
       </div>
<!-- 
       <div class="container m-4">
           <div class="row">
               <div class="col-lg-12">
                    <div class="card">
                        <div class="card-title">
                             
                        </div>
                        <div class="card-body p-4" id="resbox">
                            
                            
                        
                        </div>
                    </div>

               </div>
           </div>
       </div> -->
      

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
  </body>
</html>