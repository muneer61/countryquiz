<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Country;
use App\Continent;
use DB;

class StudentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
 

    public function index()
    {
        $continents=Continent::all();

        $randomCountry = DB::table('countries')
                        ->inRandomOrder()
                        ->first();
        return view('student.test')->with('country',$randomCountry)
                                   ->with('continents',$continents);;
    }

    public function check(Request $request,$id){
        if($request->ans==$id){
            echo("correct");
        }
        else{
            echo('incorrect');
        }

    }

}
